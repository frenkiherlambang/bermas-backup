<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\DocumentController;
use App\Http\Controllers\Api\V1\ImageController;
use LaravelJsonApi\Laravel\Facades\JsonApiRoute;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('v1/logout', [AuthController::class, 'logout']);
});

Route::post('v1/login', [AuthController::class, 'login']);
Route::post('v1/register', [AuthController::class, 'register']);

JsonApiRoute::server('v1')
    ->prefix('v1')
    // ->middleware('mustjson', 'auth:sanctum')
    ->namespace('App\Http\Controllers\Api\V1')
    ->resources(function ($server) {
        $server->resource('posts', PostController::class, function ($relationships) {
            $relationships->hasOne('category');
            $relationships->hasOne('department');
            $relationships->hasOne('user');
            $relationships->hasMany('images');
            $relationships->hasMany('documents');
        });
        $server->resource('categories', CategoryController::class);
        $server->resource('menus', MenuController::class);
        $server->resource('users', UserController::class);
        $server->resource('documents', DocumentController::class)->actions('-actions', function ($actions) {
            $actions->post('uploadFiles');
        });
        $server->resource('images', ImageController::class);
    });