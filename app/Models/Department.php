<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use HasFactory, UuidTrait;
    

    public function users()
    {
        return $this->belongsToMany(User::class, 'department_user');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }
}
