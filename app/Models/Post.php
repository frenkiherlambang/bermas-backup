<?php

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Post extends Model
{
    use HasFactory, UuidTrait;

    protected $guarded = ['id'];


    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            // $model->user_id = auth()->id;
            $model->slug = Str::slug(rand(100,999).'-'.$model->title);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
    public function documents()
    {
        return $this->hasMany(Document::class);
    }
    public function images()
    {
        return $this->hasMany(Image::class);
    }
    public function postMetas()
    {
        return $this->hasMany(PostMeta::class);
    }
}
