<?php
namespace App\Traits;

use Illuminate\Support\Str;

trait UuidTrait
    {
        public function getRouteKeyName()
        {
            return 'uuid';
        }

        public static function bootUuidTrait()
        {
            self::creating(function ($model) {
                $model->uuid = (string) Str::uuid();
            });
        }
    }
