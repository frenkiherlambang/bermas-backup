<?php
namespace App\Traits;

use Illuminate\Support\Str;

trait SearchTrait

    {
        public function scopeSearch($query, $searchTerm) {
            foreach($this->searchable as $key => $column){
                if($key == 0)
                {
                    $query->where($column, 'LIKE', '%' . $searchTerm . '%');
                }  else {
                    $query->orWhere($column, 'LIKE', '%' . $searchTerm . '%');
                }
            }
            return $query;
        }
    }
