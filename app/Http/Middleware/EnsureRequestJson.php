<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use LaravelJsonApi\Core\Document\Error;

class EnsureRequestJson
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!$request->wantsJson())
        {
            $error = Error::fromArray([
                'status' => 412,
                'detail' => 'Unsupported Media Type. Request must Accept : application/vnd.api+json',
              ]);
              return $error;
        }
        return $next($request);
    }
}
