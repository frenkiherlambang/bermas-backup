<?php

namespace App\Http\Controllers\Api\V1;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterRequest;

class AuthController extends Controller
{
    use ApiResponser;

    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->all()['data']['attributes']['name'],
            'email' => $request->all()['data']['attributes']['email'],
            'password' => Hash::make($request->all()['data']['attributes']['password']),
        ]);

        $token = $user->createToken('bermastoken')->plainTextToken;

        $response =
            [
                'user' => $user->makeHidden(['id', 'password', 'remember_token'])->toArray(),
                'token' => $token,
            ];
            return $this->success([
                'token' => $user->createToken('bermastoken')->plainTextToken,
                'user' => $user->makeHidden(['id', 'password', 'remember_token'])->toArray()
            ],'Registration successful', 200, 'Created');
    }

    public function login(LoginRequest $request)
    {
        $credentials = [
            'email' => $request->all()['data']['attributes']['email'],
            'password' => $request->all()['data']['attributes']['password'],
        ];
        if (!Auth::attempt($credentials)) {
            return $this->error('Username dan Password tidak cocok', 401, 'login failed', 'Unauthenticated');
        }
        $user = User::where('email', $credentials['email'])->first();

        return $this->success([
            'token' => $user->createToken('bermastoken')->plainTextToken,
            'user' => $user->makeHidden(['id', 'password', 'remember_token'])->toArray()
        ],'Login Succeed', 200, 'OK');
    }

    public function logout(Request $request)
    {
        $user = User::find(auth()->id());
        $user->tokens()->delete();
        return [
            'message' => 'Logged Out'
        ];
    }
}
