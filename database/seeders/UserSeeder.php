<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->has(Department::factory()->state([
            'name' => 'Bag. Kebijakan',
            'description' => 'Bagian Kebijakan',
        ]))->create([
            'email' => 'adminbagkebijakan@bermas.jogjaprov.go.id',
        ]);

        User::factory()->has(Department::factory()->state([
            'name' => 'Bag. Kelembagaan',
            'description' => 'Bagian Kelembagaan',
        ]))->create([
            'email' => 'adminbagkelembagaan@bermas.jogjaprov.go.id',
        ]);

        User::factory()->has(Department::factory()->state([
            'name' => 'Bag. Rekayasa Sosial',
            'description' => 'Bagian Rekayasa Sosial',
        ]))->create([
            'email' => 'adminbagrekayasasosial@bermas.jogjaprov.go.id',
        ]);
    
    }
}
