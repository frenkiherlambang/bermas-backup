<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $beranda = Menu::create([
            'name' => 'Beranda',
            'parent_id' => null,
            'icon' => '',
            'url' => '/',
            'type' => 'menu',
        ]);

        $profil = Menu::create([
            'name' => 'Profil',
            'parent_id' => null,
            'icon' => '',
            'url' => '/profil',
            'type' => 'menu',
        ]);

        $infoPublik = Menu::create([
            'name' => 'Info Publik',
            'parent_id' => null,
            'icon' => '',
            'url' => null,
            'type' => 'group',
        ]);

            $informasiBerkala = Menu::create([
                'name' => 'Informasi Berkala',
                'parent_id' => $infoPublik->id,
                'icon' => '',
                'url' => '/info-publik/informasi-berkala',
                'type' => 'menu',
            ]);
            $informasiSertaMerta = Menu::create([
                'name' => 'Informasi Serta Merta',
                'parent_id' => $infoPublik->id,
                'icon' => '',
                'url' => '/info-publik/informasi-serta-merta',
                'type' => 'menu',
            ]);
            $informasiSetiapSaat = Menu::create([
                'name' => 'Informasi Setiap Saat',
                'parent_id' => $infoPublik->id,
                'icon' => '',
                'url' => '/info-publik/informasi-setiap-saat',
                'type' => 'menu',
            ]);
            $informasiDikecualikan = Menu::create([
                'name' => 'Informasi Dikecualikan',
                'parent_id' => $infoPublik->id,
                'icon' => '',
                'url' => '/info-publik/informasi-berkala',
                'type' => 'menu',
            ]);
            $dokumenTerkait = Menu::create([
                'name' => 'Dokumen Terkait',
                'parent_id' => $infoPublik->id,
                'icon' => '',
                'url' => '/info-publik/dokumen-terkait',
                'type' => 'menu',
            ]);
        $agendaKegiatan = Menu::create([
            'name' => 'Agenda Kegiatan',
            'parent_id' => null,
            'icon' => '',
            'url' => '/agenda-kegiatan',
            'type' => 'menu',
        ]);
        $produk = Menu::create([
            'name' => 'Produk',
            'parent_id' => null,
            'icon' => '',
            'url' => '/produk',
            'type' => 'menu',
        ]);

        $peraturanPerundangan = Menu::create([
            'name' => 'Peraturan Perundangan',
            'parent_id' => null,
            'icon' => '',
            'url' => '/peraturan-perundangan',
            'type' => 'menu',
        ]);
    }
}
