<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'Pergub',
            'description' => 'Pergub',
        ]);

        Category::create([
            'name' => 'Surat Edaran',
            'description' => 'Surat Edaran',
        ]);

        Category::create([
            'name' => 'Surat Keputusan',
            'description' => 'Surat Keputusan',
        ]);
    }
}
