<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use App\Models\Department;
use App\Models\Document;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Post::factory()
        ->count(3)
        ->for(User::find(1))
        ->for(Category::find(1))
        ->for(Department::find(1))
        ->createQuietly();

        Post::factory()
        ->count(3)
        ->for(User::find(2))
        ->for(Category::find(2))
        ->for(Department::find(1))
        ->has(Document::factory()->count(3))
        ->createQuietly();

        Post::factory()
        ->count(3)
        ->for(User::find(3))
        ->for(Category::find(3))
        ->for(Department::find(1))
        ->createQuietly();
    }
}
