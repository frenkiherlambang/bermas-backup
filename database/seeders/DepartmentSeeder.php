<?php

namespace Database\Seeders;

use App\Models\Department;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Department::factory()->create([
            'name' => 'Bag. Kebijakan',
            'description' => 'Bagian Kebijakan',
        ]);

        Department::factory()->create([
            'name' => 'Bag. Kelembagaan',
            'description' => 'Bagian Kelembagaan',
        ]);

        Department::factory()->create([
            'name' => 'Bag. Rekayasa Sosial',
            'description' => 'Rekayasa Sosial',
        ]);
    }
}
